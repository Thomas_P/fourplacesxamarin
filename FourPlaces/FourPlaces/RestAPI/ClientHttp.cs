﻿using FourPlaces.Modèles;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace FourPlaces.RestAPI
{
    class ClientHttp
    {
        private HttpClient client;
        private LoginResult loginResult;

        public ClientHttp()
        {
            client = new HttpClient();
        }

        static async void CreateUser(String email, String password)
        {/*
            try {
                HttpResponseMessage response = await _client.PostAsync("https://td-api.julienmialon.com/auth/login");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
            }catch(ArgumentNullException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
            catch(HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }*/

        }

        public async Task PostPlace(Place place)
        {
            var uri = new Uri(String.Format("https://td-api.julienmialon.com/places", string.Empty));
            var json = JsonConvert.SerializeObject(place);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                string res = await response.Content.ReadAsStringAsync();
            }
            else
            {
                Debug.WriteLine(response.StatusCode.ToString() + response.RequestMessage);
            }
        }

        public async Task PostCommentaire(int id, string comment)
        {
            var uri = new Uri(String.Format("https://td-api.julienmialon.com/places/" + id + "/comments", string.Empty));
            string json = "{\n\"text\":\"" + comment + "\"\n}";
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync(uri, content);

            if (response.IsSuccessStatusCode)
            {
                string res = await response.Content.ReadAsStringAsync();
            }
        }


        public async Task PatchProfil(string prenom, string nom, int image_id)
        {
            var uri = new Uri(String.Format("https://td-api.julienmialon.com/me/", string.Empty));

            var json = "{\"first_name\": \"" + prenom + "\", \"last_name\": \"" + nom + "\", \"image_id\":" + image_id + "}";
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var method = new HttpMethod("PATCH");
            var request = new HttpRequestMessage(method, uri) { Content = content };

            HttpResponseMessage response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                string res = await response.Content.ReadAsStringAsync();
            }
        }

        public async Task<bool> PatchPass(string lastPSW, string newPSW)
        {
            var uri = new Uri(String.Format("https://td-api.julienmialon.com/me/password", string.Empty));

            var json = "{\"old_password\": \"" + lastPSW + "\", \"new_password\": \"" + newPSW + "\"}";
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var method = new HttpMethod("PATCH");
            var request = new HttpRequestMessage(method, uri) { Content = content };

            HttpResponseMessage response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                string res = await response.Content.ReadAsStringAsync();
                return true;
            }
            return false;
        }




        public async Task<List<PlaceItemSummary>> GetPlaces()
        {
            var uri = new Uri(String.Format("https://td-api.julienmialon.com/places/", string.Empty));
            var reponse = await client.GetAsync(uri);
            List<PlaceItemSummary> myList = new List<PlaceItemSummary>();
            if (reponse.IsSuccessStatusCode)
            {
                
                var content = await reponse.Content.ReadAsStringAsync();
                myList = JsonConvert.DeserializeObject<Response<List<PlaceItemSummary>>>(content).Data;
                
            }
            return myList;
        }



        public async Task<PlaceItemSummary> GetPlace(int id)
        {
            var uri = new Uri(String.Format("https://td-api.julienmialon.com/places/" + id, string.Empty));
            var reponse = await client.GetAsync(uri);
            PlaceItemSummary res = null;

            if (reponse.IsSuccessStatusCode)
            {
                var content = await reponse.Content.ReadAsStringAsync();
                res = JsonConvert.DeserializeObject<Response<PlaceItemSummary>>(content).Data;
            }
            return res;
        }

        public async Task<List<string>> GetAllImages()
        {
            List<string> lesImages = new List<string>();
            bool thereIsMoreImage = true;
            int imageId = 1;
            while (thereIsMoreImage)
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "https://td-api.julienmialon.com/images/" + imageId);
                HttpResponseMessage response = await client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    lesImages.Add("https://td-api.julienmialon.com/images/" + imageId);
                    imageId++;
                }
                else
                {
                    Debug.WriteLine(response.StatusCode);
                    thereIsMoreImage = false;
                }
            }
            return lesImages;
        }

        public async Task<bool> getImageById(string imageID)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, imageID);
            HttpResponseMessage response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> createAccount(string email, string prenom, string nom, string password)
        {
            var uri = new Uri(String.Format("https://td-api.julienmialon.com/auth/register", string.Empty));
            var json = "{\"email\":\"" + email + "\",\"first_name\":\"" + prenom + "\",\"last_name\":\"" + nom + "\",\"password\":\"" + password + "\"}";
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync(uri, content);

            if (response.IsSuccessStatusCode)
            {
                string res = await response.Content.ReadAsStringAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> connexion(string email, string password)
        {
            var uri = new Uri(string.Format("https://td-api.julienmialon.com/auth/login", string.Empty));
            var json = "{\"email\":\"" + email + "\",\"password\":\"" + password + "\"}";
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync(uri, content);
            if (response.IsSuccessStatusCode)
            {
                string res = await response.Content.ReadAsStringAsync();
                loginResult = JsonConvert.DeserializeObject<Response<LoginResult>>(res).Data;
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(loginResult.TokenType, loginResult.AccessToken);
                return true;
            }
            return false;
        }

    }

}
