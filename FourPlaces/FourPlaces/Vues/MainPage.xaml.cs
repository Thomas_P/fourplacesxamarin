﻿using FourPlaces.Modèles;
using FourPlaces.RestAPI;
using FourPlaces.VM;
using Storm.Mvvm.Forms;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FourPlaces.Vues
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(true)]
    public partial class MainPage : BaseContentPage
    {
        INavigation nav;
        public MainPage(INavigation nav)
        {
            this.nav = nav;
            InitializeComponent();
            BindingContext = new VMMain();
        }

        public async void OnItemTappedAsync(object sender, ItemTappedEventArgs e)
        {
            PlaceItemSummary item = e.Item as PlaceItemSummary;
            if(item != null)
            {
                try
                {
                    await nav.PushAsync(new NavigationPage(new ViewItem(item)));
                }catch(Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
        }
    }
}
