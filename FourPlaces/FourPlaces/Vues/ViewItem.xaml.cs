﻿using FourPlaces.Modèles;
using FourPlaces.VM;
using Storm.Mvvm.Forms;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace FourPlaces.Vues
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ViewItem : ContentPage
    {
        private PlaceItemSummary placeSelected;
        public ViewItem(PlaceItemSummary place)
        {
            Debug.WriteLine(place.Description);
            InitializeComponent();
            placeSelected = place;
            BindingContext = new VMItem(place);
            Xamarin.Forms.Maps.Position mypos = new Xamarin.Forms.Maps.Position(place.Latitude, place.Longitude);
            mymap.MoveToRegion(MapSpan.FromCenterAndRadius(mypos, Distance.FromKilometers(5)));
            var pin = new Pin()
            {
                Type = PinType.Place,
                Position = mypos,
                Label = place.Title
            };
            mymap.Pins.Add(pin);
        }
        
    }
}