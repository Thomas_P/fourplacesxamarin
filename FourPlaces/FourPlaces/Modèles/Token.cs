﻿using System;

namespace FourPlaces.Modèles
{
    public class Token
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }

        public DateTime ExpirationDate { get; set; }
    }
}
