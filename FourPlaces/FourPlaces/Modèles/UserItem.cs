namespace FourPlaces.Mod�les
{
    public class UserItem
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int? ImageId { get; set; }
    }
}