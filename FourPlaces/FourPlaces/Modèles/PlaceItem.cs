using System.Collections.Generic;
namespace FourPlaces.Mod�les
{
	public class PlaceItem
	{
		public int Id { get; set; }
		
		public string Title { get; set; }
		
		public string Description { get; set; }
		
		public int ImageId { get; set; }
		
		public double Latitude { get; set; }
		
		public double Longitude { get; set; }
		
		public List<CommentItem> Comments { get; set; }
	}
}