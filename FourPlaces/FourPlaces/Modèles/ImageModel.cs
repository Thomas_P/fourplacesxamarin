﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FourPlaces.Modèles
{
    public class ImageModel
    {

        public int Id { get; set; }

        public byte[] Data { get; set; }
    }
}