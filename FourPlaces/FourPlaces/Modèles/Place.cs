﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FourPlaces.Modèles
{
    public class Place
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int ImageId { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }
    }
}
