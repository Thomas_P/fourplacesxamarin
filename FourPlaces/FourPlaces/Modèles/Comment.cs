﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FourPlaces.Modèles
{
    public class Comment
    {
        public int Id { get; set; }

        public int PlaceId { get; set; }

        public int AuthorId { get; set; }

        public string Text { get; set; }
    }
}