namespace FourPlaces.Mod�les
{
	public class CreatePlaceRequest
	{
		public string Title { get; set; }
		
		public string Description { get; set; }
		
		public int ImageId { get; set; }
		
		public double Latitude { get; set; }
		
		public double Longitude { get; set; }
	}
}