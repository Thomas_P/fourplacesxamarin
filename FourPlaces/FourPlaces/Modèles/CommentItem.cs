using System;

namespace FourPlaces.Mod�les
{
	public class CommentItem
	{
		public DateTime Date { get; set; }
		
		public UserItem Author { get; set; }
		
		public string Text { get; set; }
	}
}