namespace FourPlaces.Mod�les
{
    public class UpdateProfileRequest
    {
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public int? ImageId { get; set; }
    }
}