﻿using FourPlaces.Modèles;
using FourPlaces.Vues;
using Plugin.Geolocator;
using Plugin.Permissions;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms.Maps;

namespace FourPlaces.VM
{
    class VMItem : ViewModelBase
    {
        private int _distance;

        public int Distance
        {
            get => _distance;
            set => SetProperty(ref _distance, value);
        }

        public Plugin.Geolocator.Abstractions.Position pos;

        private PlaceItemSummary _lieu = new PlaceItemSummary();

        public PlaceItemSummary Lieu
        {
            get => _lieu;
            set => SetProperty(ref _lieu, value);
        }


        public VMItem(PlaceItemSummary place)
        {
            Lieu = place;
            TestAsync();
            

        }

        private async Task<bool> TestAsync()
        {
            GetCurrentPosition();
            Debug.WriteLine("location : " + pos);
            return true;
        }

        public async void GetCurrentPosition()
        {
            var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Plugin.Permissions.Abstractions.Permission.Location);

            if (status != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
            {
                var results = await CrossPermissions.Current.RequestPermissionsAsync(Plugin.Permissions.Abstractions.Permission.Location);
                if (results.ContainsKey(Plugin.Permissions.Abstractions.Permission.Location))
                {
                    status = results[Plugin.Permissions.Abstractions.Permission.Location];
                }
            }

            if (status == Plugin.Permissions.Abstractions.PermissionStatus.Granted)
            {
                Plugin.Geolocator.Abstractions.Position myPos = null;
                try
                {
                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 100;
                    myPos = await locator.GetLastKnownLocationAsync();
                    if (myPos != null)
                    {
                        
                        pos = myPos;
                    }
                    else
                    {
                        await App.Current.MainPage.DisplayAlert("Unable to locate", "Erreur de localisation", "OK");
                    }
                }
                catch
                {
                    await App.Current.MainPage.DisplayAlert("Unable to locate", "Erreur de localisation", "OK");
                }
            }
        }
    }
}
