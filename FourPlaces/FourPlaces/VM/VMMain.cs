﻿using FourPlaces.Modèles;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Storm.Mvvm;
using FourPlaces.RestAPI;
using System.Threading.Tasks;
using System.Diagnostics;
using FourPlaces.Vues;
using Xamarin.Forms;
using Storm.Mvvm.Services;

namespace FourPlaces.VM
{
    class VMMain : ViewModelBase
    {
        private ObservableCollection<PlaceItemSummary> _lieuList = new ObservableCollection<PlaceItemSummary>();
        public ObservableCollection<PlaceItemSummary> ListeLieux
        {
            get => _lieuList;
            set => SetProperty(ref _lieuList, value);

        }
        public VMMain()
        {
            TestAsync();
        }

        private async Task<Boolean> TestAsync()
        {
            ClientHttp monServ = new ClientHttp();
            List<PlaceItemSummary> l = await monServ.GetPlaces();
            foreach (var place in l)
            {
                _lieuList.Add(place);
            }
            return true;
        }
    }
}
