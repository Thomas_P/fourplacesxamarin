﻿using FourPlaces.RestAPI;
using FourPlaces.Vues;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace FourPlaces.VM
{
    class VMLogin : ViewModelBase
    {

        private string _name;

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }
        private string _firstname;

        public string Firstname
        {
            get => _firstname;
            set => SetProperty(ref _firstname, value);
        }

        private string _emailC;

        public string EmailC
        {
            get => _emailC;
            set => SetProperty(ref _emailC, value);
        }

        private string _pswC;

        public string PswC
        {
            get => _pswC;
            set => SetProperty(ref _pswC, value);
        }

        private string _email;

        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        private string _psw;

        public string Psw
        {
            get => _psw;
            set => SetProperty(ref _psw, value);
        }
        public ICommand ConnectCommand
        {
            get;set;
        }

        public ICommand CreateCommand
        {
            get; set;
        }

        public VMLogin(INavigation nav)
        {
            ConnectCommand = new Command(()=> TestConnexion(Email,Psw, nav));
            CreateCommand = new Command(() => CreateAccount(EmailC, PswC, Name, Firstname, nav));
        }

        private async Task<Boolean> TestConnexion(string email, string psw, INavigation nav)
        {
            ClientHttp monServ = new ClientHttp();
            bool res = await monServ.connexion(email, psw);
            if (res == true)
            {
                await nav.PushAsync(new NavigationPage(new MainPage(nav)));
                return true;
            }
            return true;
        }

        private async Task<Boolean> CreateAccount(string email, string psw, string name, string firstname, INavigation nav)
        {
            ClientHttp monServ = new ClientHttp();
            bool res=await monServ.createAccount(email, firstname, name, psw);
            if (res == true)
            {
                await nav.PushAsync(new NavigationPage(new MainPage(nav)));
                return true;
            }
            return true;
        }
    }
}
